---
'@primer/react': patch
---

AnchoredOverlay: Add support for passing an id to the anchor. Remove unnecessary aria-labelledby on anchor.
ActionMenu v2: Add aria-labelledby for ActionList
